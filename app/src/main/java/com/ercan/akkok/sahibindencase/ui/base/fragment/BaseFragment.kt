package com.ercan.akkok.sahibindencase.ui.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.ercan.akkok.sahibindencase.R

abstract class BaseFragment : Fragment() {

    protected open val binding: ViewBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return when {
            binding != null -> binding!!.root
            getLayoutId() != null -> inflater.inflate(getLayoutId()!!, container, false)
            else -> throw RuntimeException("At least binding or layout id should not be null")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
    }

    protected val navigationId: Int = Int.MIN_VALUE

    /**
     * Method to get activity's UI content layout resource id.
     *
     * @return The activity's content's resource id
     */
    protected open fun getLayoutId(): Int? = null

    /**
     * Initialize UI content elements.
     */
    protected abstract fun initViews(view: View)

    open fun showLoading() = Unit

    open fun hideLoading() = Unit

    open fun showError(error: Throwable) {
        AlertDialog.Builder(requireContext())
            .setTitle("Error")
            .setMessage(error.message)
            .setPositiveButton("OK") { _, _ ->
                findNavController().popBackStack()
            }
            .show()
    }

}
