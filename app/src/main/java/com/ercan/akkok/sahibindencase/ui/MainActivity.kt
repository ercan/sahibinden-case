package com.ercan.akkok.sahibindencase.ui

import com.ercan.akkok.sahibindencase.databinding.ActivityMainBinding
import com.ercan.akkok.sahibindencase.ui.base.activity.BaseActivity
import com.ercan.akkok.sahibindencase.ui.base.activity.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    override val binding by viewBinding(ActivityMainBinding::inflate)

}
