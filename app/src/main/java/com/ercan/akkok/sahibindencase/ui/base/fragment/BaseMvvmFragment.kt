package com.ercan.akkok.sahibindencase.ui.base.fragment

import android.os.Bundle
import android.view.View
import com.ercan.akkok.sahibindencase.ui.base.mvvm.BaseViewModel
import com.ercan.akkok.sahibindencase.ui.base.mvvm.MvvmDelegate

abstract class BaseMvvmFragment<VM : BaseViewModel> : BaseFragment(), MvvmDelegate<VM> {

    override var _viewModel: VM? = null

    protected val viewModel: VM
        get() = _getViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (_viewModel == null){
            attach()
        }
    }

    override fun onDestroyView() {
        detach()
        super.onDestroyView()
    }
}
