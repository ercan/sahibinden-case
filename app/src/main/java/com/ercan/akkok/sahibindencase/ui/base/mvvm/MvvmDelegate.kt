package com.ercan.akkok.sahibindencase.ui.base.mvvm

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import com.ercan.akkok.sahibindencase.ui.base.activity.BaseActivity
import com.ercan.akkok.sahibindencase.ui.base.fragment.BaseFragment
import java.lang.reflect.ParameterizedType

/**
 * A delegation class responsible for creating [ViewModel] class.
 * <p>
 * [ViewModel] is created in [attach] and is detached in [detach].
 *
 * @param [ViewModel] The type of [BaseViewModel]
 */
interface MvvmDelegate<ViewModel : BaseViewModel> {
    var _viewModel: ViewModel?

    /**
     * Call this function when the view is creating
     */
    @CallSuper
    fun attach() {
        observeEvents()
        _getViewModel().onViewAttached()
        onViewModelAttached()
    }

    /**
     * Call this function when the view is destroying
     */
    @CallSuper
    fun detach() {
        onViewModelDetached()
        _getViewModel().onViewDetached()
    }

    /**
     * Create the view model instance.
     *
     * @return the created view model instance
     */
    fun provideViewModel(): ViewModel {
        val viewModelStore: ViewModelStore
        val viewModelProviderFactory: ViewModelProvider.Factory
        when (this) {
            is BaseActivity -> {
                viewModelStore = this.viewModelStore
                viewModelProviderFactory = this.defaultViewModelProviderFactory
            }
            is BaseFragment -> {
                viewModelStore = this.viewModelStore
                viewModelProviderFactory = this.defaultViewModelProviderFactory
            }
            else -> throw RuntimeException("To provide ViewModel, you should extend BaseActivity or BaseFragment")
        }

        return ViewModelProvider(viewModelStore, viewModelProviderFactory).get(getViewModelClass())
    }

    /**
     * Set the view model instance
     *
     * @param viewModel The view model instance
     */
    fun setViewModel(viewModel: ViewModel) {
        _viewModel = viewModel
    }

    /**
     * Get the view model. If it is null, then a internally a new view model instance
     * gets created by calling [provideViewModel]
     *
     * @return the view model instance
     */
    fun _getViewModel(): ViewModel {
        if (_viewModel == null) {
            _viewModel = provideViewModel()
        }
        return _viewModel!!
    }

    /**
     * Observes live data
     */
    fun observeEvents() {
        _getViewModel().let {
            when (this) {
                is BaseActivity -> {
                    it.loading.observe(this) { loading ->
                        if (loading)
                            showLoading()
                        else
                            hideLoading()
                    }
                    it.error.observe(this) { error ->
                        showError(error)
                    }
                }
                is BaseFragment -> {
                    it.loading.observe(this) { loading ->
                        if (loading)
                            showLoading()
                        else
                            hideLoading()
                    }
                    it.error.observe(this) { error ->
                        showError(error)
                    }
                }
            }
        }
    }

    /**
     * Override this on child view if needed.
     */
    fun onViewModelAttached() = Unit

    /**
     * Override this on child view if needed.
     */
    fun onViewModelDetached() = Unit
}

fun <ViewModel : BaseViewModel> Any.getViewModelClass(): Class<ViewModel> {
    @Suppress("UNCHECKED_CAST")
    return (javaClass.genericSuperclass as ParameterizedType)
        .actualTypeArguments.first() as Class<ViewModel>
}
