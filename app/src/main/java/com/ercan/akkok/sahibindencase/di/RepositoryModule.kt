package com.ercan.akkok.sahibindencase.di

import com.ercan.akkok.data.repository.user.UserDataRepository
import com.ercan.akkok.domain.user.repository.UserRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Singleton
    @Binds
    fun bindUserRepository(dataRepository: UserDataRepository): UserRepository
}
