package com.ercan.akkok.sahibindencase.ui.user.detail

import com.ercan.akkok.domain.user.model.UserDetail
import com.ercan.akkok.domain.user.usecase.GetUserDetail
import com.ercan.akkok.sahibindencase.ui.base.mvvm.BaseViewModel
import com.ercan.akkok.sahibindencase.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserDetailViewModel @Inject constructor(
    private val getUserDetail: GetUserDetail
) : BaseViewModel() {

    val userDetail = SingleLiveEvent<UserDetail>()

    fun getUserDetail(username: String) {
        getUserDetail(this) {
            onResponse = {
                userDetail.value = it
            }
            params = GetUserDetail.Params(username)
        }
    }
}
