package com.ercan.akkok.sahibindencase.ui.user.input

import com.ercan.akkok.domain.user.model.UserDetail
import com.ercan.akkok.domain.user.usecase.GetUserDetail
import com.ercan.akkok.sahibindencase.ui.base.mvvm.BaseViewModel
import com.ercan.akkok.sahibindencase.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UsernameInputViewModel @Inject constructor(
    private val getUserDetail: GetUserDetail
) : BaseViewModel() {

    val userDetail = SingleLiveEvent<UserDetail>()
    val message = SingleLiveEvent<String>()

    fun getUserDetails(username: String) {
        getUserDetail(this) {
            onResponse = {
                if (it.isProtected) {
                    message.value = "This username is protected, please enter public username."
                } else {
                    userDetail.value = it
                }
            }
            params = GetUserDetail.Params(username)
        }
    }
}
