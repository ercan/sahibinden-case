package com.ercan.akkok.sahibindencase.ui.base.mvvm

import androidx.lifecycle.ViewModel
import com.ercan.akkok.domain.base.UseCaseRunner
import com.ercan.akkok.domain.base.UseCaseRunnerDelegate
import com.ercan.akkok.domain.util.Callback
import com.ercan.akkok.sahibindencase.util.SingleLiveEvent

abstract class BaseViewModel : ViewModel(),
    UseCaseRunner by UseCaseRunnerDelegate() {

    val loading = SingleLiveEvent<Boolean>()
    val error = SingleLiveEvent<Throwable>()

    open fun onViewAttached() = Unit

    open fun onViewDetached() = Unit

    override fun <T> callback(
        showLoading: Boolean,
        onStart: (() -> Unit)?,
        onResponse: (T) -> Unit,
        onError: ((Throwable) -> Unit)?,
        onComplete: (() -> Unit)?
    ): Callback<T> {
        return Callback(
            onStart = {
                if (onStart != null) {
                    onStart()
                } else {
                    if (showLoading) {
                        loading.value = true
                    }
                }
            },
            onResponse = {
                if (showLoading) {
                    loading.value = false
                }
                onResponse(it)
            },
            onError = {
                if (onError != null) {
                    onError(it)
                } else {
                    error.value = it
                }
            },
            onComplete = {
                if (onComplete != null) {
                    onComplete()
                } else {
                    if (showLoading) {
                        loading.value = false
                    }
                }
            }
        )
    }
}
