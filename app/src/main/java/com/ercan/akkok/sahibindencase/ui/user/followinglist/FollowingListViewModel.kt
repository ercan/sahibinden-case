package com.ercan.akkok.sahibindencase.ui.user.followinglist

import com.ercan.akkok.domain.user.model.FollowingUser
import com.ercan.akkok.domain.user.usecase.GetFollowingList
import com.ercan.akkok.sahibindencase.ui.base.mvvm.BaseViewModel
import com.ercan.akkok.sahibindencase.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FollowingListViewModel @Inject constructor(
    private val getFollowingList: GetFollowingList
) : BaseViewModel() {

    val followingList = SingleLiveEvent<List<FollowingUser>>()

    fun getFollowingList(userId: String) {
        getFollowingList(this) {
            onResponse = {
                followingList.value = it
            }
            params = GetFollowingList.Params(userId)
        }
    }

}
