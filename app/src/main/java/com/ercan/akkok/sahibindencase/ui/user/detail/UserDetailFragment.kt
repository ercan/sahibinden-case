package com.ercan.akkok.sahibindencase.ui.user.detail

import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.ercan.akkok.sahibindencase.databinding.FragmentUserDetailBinding
import com.ercan.akkok.sahibindencase.ui.base.fragment.BaseMvvmFragment
import com.ercan.akkok.sahibindencase.ui.base.fragment.viewBinding
import com.ercan.akkok.sahibindencase.util.px
import dagger.hilt.android.AndroidEntryPoint
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

@AndroidEntryPoint
class UserDetailFragment : BaseMvvmFragment<UserDetailViewModel>() {

    override val binding by viewBinding(FragmentUserDetailBinding::inflate)

    private val args: UserDetailFragmentArgs by navArgs()

    override fun initViews(view: View) {
        binding.imageViewClose.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onViewModelAttached() {
        viewModel.getUserDetail(args.userTag)
    }

    override fun observeEvents() {
        super.observeEvents()

        viewModel.userDetail.observe(this) {
            binding.textViewUserTag.text = it.tag
            binding.textViewUsername.text = it.name
            Glide.with(this)
                .load(it.imageUrl.replace("http", "https"))
                .apply(RequestOptions.bitmapTransform(RoundedCornersTransformation(5.px, 0)))
                .apply(RequestOptions().override(600, 200))
                .fitCenter()
                .into(binding.imageViewUser)
        }
    }

    override fun showLoading() {
        binding.progressBar.isVisible = true
    }

    override fun hideLoading() {
        binding.progressBar.isVisible = false
    }

}
