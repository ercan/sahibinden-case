package com.ercan.akkok.sahibindencase.util

import android.content.res.Resources
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View

fun View.inflater(): LayoutInflater = LayoutInflater.from(context)

/**
 * Dimension
 */
val Number.px: Int
    get() = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        Resources.getSystem().displayMetrics
    ).toInt()
