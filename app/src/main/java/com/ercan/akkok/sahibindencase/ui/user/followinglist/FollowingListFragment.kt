package com.ercan.akkok.sahibindencase.ui.user.followinglist

import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ercan.akkok.domain.user.model.FollowingUser
import com.ercan.akkok.sahibindencase.databinding.FragmentFollowingListBinding
import com.ercan.akkok.sahibindencase.ui.adapter.followinguser.FollowingUserRecyclerAdapter
import com.ercan.akkok.sahibindencase.ui.adapter.followinguser.FollowingUserRecyclerCallback
import com.ercan.akkok.sahibindencase.ui.base.fragment.BaseMvvmFragment
import com.ercan.akkok.sahibindencase.ui.base.fragment.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FollowingListFragment : BaseMvvmFragment<FollowingListViewModel>(),
    FollowingUserRecyclerCallback {

    override val binding by viewBinding(FragmentFollowingListBinding::inflate)

    private val args: FollowingListFragmentArgs by navArgs()

    private val followingUserRecyclerAdapter by lazy {
        FollowingUserRecyclerAdapter(this)
    }

    override fun initViews(view: View) {
        binding.recyclerViewFollowingUsers.adapter = followingUserRecyclerAdapter
        binding.imageViewClose.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    override fun onViewModelAttached() {
        viewModel.getFollowingList(args.userId)
    }

    override fun observeEvents() {
        super.observeEvents()

        viewModel.followingList.observe(this) {
            followingUserRecyclerAdapter.setItems(it)
        }
    }

    override fun showLoading() {
        binding.progressBar.isVisible = true
    }

    override fun hideLoading() {
        binding.progressBar.isVisible = false
    }

    override fun onUserClick(followingUser: FollowingUser) {
        findNavController().navigate(
            FollowingListFragmentDirections.actionUserContracts(
                followingUser.tag
            )
        )
    }

}
