package com.ercan.akkok.sahibindencase.ui.base.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity : AppCompatActivity() {

    protected open val binding: ViewBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        when {
            binding != null -> setContentView(binding!!.root)
            getLayoutId() != null -> setContentView(getLayoutId()!!)
            else -> throw RuntimeException("At least binding or layout id should not be null")
        }

        initViews()
    }

    /**
     * Method to get activity's UI content layout resource id.
     *
     * @return The activity's content's resource id
     */
    protected open fun getLayoutId(): Int? = null

    /**
     * Initialize UI content elements.
     */
    protected open fun initViews() = Unit

    open fun showLoading() = Unit

    open fun hideLoading() = Unit

    open fun showError(error: Throwable) {
        hideLoading()
        AlertDialog.Builder(this)
            .setTitle("Error")
            .setMessage("Would you want to reload this screen?")
            .setPositiveButton("Yes") { _, _ ->
                startActivity(Intent.makeRestartActivityTask(this.intent?.component))
            }
            .setNegativeButton("No", null)
            .show()
    }

}
