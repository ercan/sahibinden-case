package com.ercan.akkok.sahibindencase.ui.adapter.followinguser

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ercan.akkok.domain.user.model.FollowingUser
import com.ercan.akkok.sahibindencase.databinding.RecyclerItemFollowingUserBinding
import com.ercan.akkok.sahibindencase.util.inflater

class FollowingUserRecyclerAdapter(
    private val callback: FollowingUserRecyclerCallback?,
    followingUsers: List<FollowingUser> = arrayListOf()
) : RecyclerView.Adapter<FollowingUserRecyclerAdapter.ViewHolder>() {

    private val items = ArrayList<FollowingUser>()

    init {
        items.let {
            this.items.addAll(followingUsers)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RecyclerItemFollowingUserBinding
            .inflate(parent.inflater(), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(items[position]) {
                val userTag = "@${this.tag}"
                binding.textViewUserTag.text = userTag
                binding.textViewUsername.text = this.name

                itemView.setOnClickListener {
                    callback?.onUserClick(this)
                }
            }
        }
    }

    fun setItems(items: List<FollowingUser>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: RecyclerItemFollowingUserBinding) :
        RecyclerView.ViewHolder(binding.root)
}

interface FollowingUserRecyclerCallback {

    fun onUserClick(followingUser: FollowingUser)
}
