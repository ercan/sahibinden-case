package com.ercan.akkok.sahibindencase.ui.base.fragment

import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class FragmentViewBindingDelegate<ViewBinding : androidx.viewbinding.ViewBinding>(
    private val fragment: Fragment,
    private val viewBindingFactory: (LayoutInflater) -> ViewBinding
) : ReadOnlyProperty<Fragment, ViewBinding>, DefaultLifecycleObserver {

    private var binding: ViewBinding? = null

    init {
        fragment.lifecycle.addObserver(this)
        fragment.viewLifecycleOwnerLiveData.observe(fragment) { viewLifecycleOwner ->
            viewLifecycleOwner?.lifecycle?.addObserver(object : DefaultLifecycleObserver {
                override fun onDestroy(owner: LifecycleOwner) {
                    binding = null
                }
            })
        }
    }

    override fun getValue(
        thisRef: Fragment,
        property: KProperty<*>
    ): ViewBinding = binding ?: getViewBinding()

    override fun onDestroy(owner: LifecycleOwner) {
        fragment.lifecycle.removeObserver(this)
    }

    private fun getViewBinding(): ViewBinding {
        val lifecycle = fragment.viewLifecycleOwner.lifecycle
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            throw IllegalStateException("Should not attempt to get bindings when Fragment view is destroyed.")
        }

        return viewBindingFactory(fragment.layoutInflater).also {
            this.binding = it
        }
    }
}

fun <ViewBinding : androidx.viewbinding.ViewBinding> Fragment.viewBinding(
    viewBindingFactory: (LayoutInflater) -> ViewBinding
) = FragmentViewBindingDelegate(this, viewBindingFactory)
