package com.ercan.akkok.sahibindencase.ui.base.activity

import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class ActivityViewBindingDelegate<ViewBinding : androidx.viewbinding.ViewBinding>(
    private val activity: AppCompatActivity,
    private val viewBindingFactory: (LayoutInflater) -> ViewBinding
) : ReadOnlyProperty<AppCompatActivity, ViewBinding>, DefaultLifecycleObserver {

    private var binding: ViewBinding? = null

    init {
        activity.lifecycle.addObserver(this)
    }

    override fun getValue(
        thisRef: AppCompatActivity,
        property: KProperty<*>
    ): ViewBinding = binding ?: getViewBinding()

    override fun onDestroy(owner: LifecycleOwner) {
        binding = null
        activity.lifecycle.removeObserver(this)
    }

    private fun getViewBinding(): ViewBinding {
        val lifecycle = activity.lifecycle
        if (!lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
            throw IllegalStateException("Should not attempt to get bindings when Activity view is destroyed.")
        }

        return viewBindingFactory(activity.layoutInflater).also {
            this.binding = it
        }
    }
}

fun <ViewBinding : androidx.viewbinding.ViewBinding> AppCompatActivity.viewBinding(
    viewBindingFactory: (LayoutInflater) -> ViewBinding
) = ActivityViewBindingDelegate(this, viewBindingFactory)
