package com.ercan.akkok.sahibindencase

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SahibindenCaseApp : Application()
