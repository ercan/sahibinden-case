package com.ercan.akkok.sahibindencase.ui.user.input

import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.ercan.akkok.sahibindencase.databinding.FragmentUsernameInputBinding
import com.ercan.akkok.sahibindencase.ui.base.fragment.BaseMvvmFragment
import com.ercan.akkok.sahibindencase.ui.base.fragment.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UsernameInputFragment : BaseMvvmFragment<UsernameInputViewModel>() {

    override val binding by viewBinding(FragmentUsernameInputBinding::inflate)

    override fun initViews(view: View) {
        binding.buttonEnterUsername.setOnClickListener {
            viewModel.getUserDetails(binding.editTextUsername.text.toString())
        }
    }

    override fun observeEvents() {
        super.observeEvents()

        viewModel.message.observe(this) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }

        viewModel.userDetail.observe(this) {
            findNavController().navigate(UsernameInputFragmentDirections.actionUserContracts(it.id))
        }
    }

    override fun showLoading() {
        binding.progressBar.isVisible = true
    }


    override fun hideLoading() {
        binding.progressBar.isVisible = false
    }

}
