package com.ercan.akkok.data.repository.user.model

import com.google.gson.annotations.SerializedName

data class FollowingUserWrapperResponse(
    @SerializedName("data") val data: List<FollowingUserResponse>
)

data class FollowingUserResponse(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("username") val tag: String
)
