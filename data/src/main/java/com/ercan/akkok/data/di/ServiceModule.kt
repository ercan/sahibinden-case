package com.ercan.akkok.data.di

import com.ercan.akkok.data.repository.user.service.UserService
import com.ercan.akkok.data.net.ApiProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Singleton
    @Provides
    fun provideUserService(apiProvider: ApiProvider) =
        apiProvider.create(UserService::class.java)
}
