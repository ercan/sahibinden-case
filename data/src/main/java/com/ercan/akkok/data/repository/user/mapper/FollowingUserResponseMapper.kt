package com.ercan.akkok.data.repository.user.mapper

import com.ercan.akkok.data.repository.user.model.FollowingUserResponse
import com.ercan.akkok.domain.user.model.FollowingUser

fun FollowingUserResponse.mapToDomainModel() =
    FollowingUser(
        id = this.id,
        name = this.name,
        tag = this.tag
    )
