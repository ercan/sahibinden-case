package com.ercan.akkok.data.repository.user.service

import com.ercan.akkok.data.repository.user.model.FollowingUserWrapperResponse
import com.ercan.akkok.data.repository.user.model.UserDetailResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UserService {

    @GET("1.1/users/show.json")
    suspend fun getUserDetail(
        @Query("screen_name") username: String,
    ): UserDetailResponse

    @GET("2/users/{id}/following")
    suspend fun getFollowingUserList(
        @Path("id") userId: String,
    ): FollowingUserWrapperResponse
}
