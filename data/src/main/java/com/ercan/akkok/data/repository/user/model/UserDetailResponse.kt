package com.ercan.akkok.data.repository.user.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserDetailResponse(
    @SerializedName("id") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("screen_name") val tag: String,
    @SerializedName("description") val description: String,
    @SerializedName("protected") val isProtected: Boolean,
    @SerializedName("profile_image_url") val imageUrl: String,
) : Parcelable
