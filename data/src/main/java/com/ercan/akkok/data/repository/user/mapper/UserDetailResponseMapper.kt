package com.ercan.akkok.data.repository.user.mapper

import com.ercan.akkok.data.repository.user.model.UserDetailResponse
import com.ercan.akkok.domain.user.model.UserDetail

fun UserDetailResponse.mapToDomainModel() =
    UserDetail(
        id = this.id,
        name = this.name,
        tag = this.tag,
        description = this.description,
        isProtected = this.isProtected,
        imageUrl = this.imageUrl,
    )
