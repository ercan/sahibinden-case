package com.ercan.akkok.data.repository.user

import com.ercan.akkok.data.repository.user.mapper.mapToDomainModel
import com.ercan.akkok.data.repository.user.service.UserService
import com.ercan.akkok.domain.user.model.FollowingUser
import com.ercan.akkok.domain.user.model.UserDetail
import com.ercan.akkok.domain.user.repository.UserRepository
import javax.inject.Inject

class UserDataRepository @Inject constructor(
    private val userService: UserService
) : UserRepository {

    override suspend fun getUserDetails(username: String): UserDetail {
        return userService.getUserDetail(username).mapToDomainModel()
    }

    override suspend fun getFollowingList(userId: String): List<FollowingUser> {
        return userService.getFollowingUserList(userId).data.map { wrapper ->
            wrapper.mapToDomainModel()
        }
    }
}
