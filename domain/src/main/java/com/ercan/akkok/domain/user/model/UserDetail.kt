package com.ercan.akkok.domain.user.model

data class UserDetail(
    val id: String,
    val name: String,
    val tag: String,
    val description: String,
    val isProtected: Boolean,
    val imageUrl: String,
)
