package com.ercan.akkok.domain.user.usecase

import com.ercan.akkok.domain.base.CoroutineUseCase
import com.ercan.akkok.domain.user.model.UserDetail
import com.ercan.akkok.domain.user.repository.UserRepository
import javax.inject.Inject

class GetUserDetail @Inject constructor(
    private val userRepository: UserRepository
) : CoroutineUseCase<UserDetail, GetUserDetail.Params>() {

    override suspend fun buildUseCase(params: Params?): UserDetail {
        return userRepository.getUserDetails(
            username = params!!.username
        )
    }

    data class Params(
        val username: String
    )
}
