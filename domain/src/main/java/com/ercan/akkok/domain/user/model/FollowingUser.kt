package com.ercan.akkok.domain.user.model

data class FollowingUser(
    val id: String,
    val name: String,
    val tag: String
)
