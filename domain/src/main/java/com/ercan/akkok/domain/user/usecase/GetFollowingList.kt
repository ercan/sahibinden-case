package com.ercan.akkok.domain.user.usecase

import com.ercan.akkok.domain.base.CoroutineUseCase
import com.ercan.akkok.domain.user.model.FollowingUser
import com.ercan.akkok.domain.user.repository.UserRepository
import javax.inject.Inject

class GetFollowingList @Inject constructor(
    private val userRepository: UserRepository
) : CoroutineUseCase<List<FollowingUser>, GetFollowingList.Params>() {

    override suspend fun buildUseCase(params: Params?): List<FollowingUser> {
        return userRepository.getFollowingList(
            userId = params!!.userId
        )
    }

    data class Params(
        val userId: String
    )
}
