package com.ercan.akkok.domain.user.repository

import com.ercan.akkok.domain.user.model.FollowingUser
import com.ercan.akkok.domain.user.model.UserDetail

interface UserRepository {

    suspend fun getUserDetails(username: String): UserDetail

    suspend fun getFollowingList(userId: String): List<FollowingUser>
}
