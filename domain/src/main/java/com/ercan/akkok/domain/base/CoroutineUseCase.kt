package com.ercan.akkok.domain.base

import com.ercan.akkok.domain.util.Callback
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

abstract class CoroutineUseCase<T, Params> {

    private var scope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    private val dispatcher = Dispatchers.IO

    operator fun invoke(
        useCaseRunner: UseCaseRunner,
        useCaseBuilderBlock: UseCaseBuilder<T, Params>.() -> Unit
    ) = execute(useCaseRunner, useCaseBuilderBlock)

    fun execute(
        useCaseRunner: UseCaseRunner,
        useCaseBuilderBlock: UseCaseBuilder<T, Params>.() -> Unit
    ) {
        val useCaseBuilder = UseCaseBuilder<T, Params>().apply(useCaseBuilderBlock)
        execute(
            useCaseRunner = useCaseRunner,
            callback = useCaseRunner.callback(
                showLoading = useCaseBuilder.showLoading,
                onStart = useCaseBuilder.onStart,
                onResponse = useCaseBuilder.onResponse,
                onError = useCaseBuilder.onError,
                onComplete = useCaseBuilder.onComplete
            ),
            params = useCaseBuilder.params
        )
    }

    private fun execute(
        useCaseRunner: UseCaseRunner,
        callback: Callback<T>?,
        params: Params?
    ) {
        useCaseRunner.subscribeToCancel {
            cancel()
        }

        scope.launch {
            flow {
                runCatching {
                    buildUseCase(params)
                }.onSuccess {
                    emit(Result.Success(it))
                }.onFailure {
                    emit(Result.Error(it))
                }
            }
                .flowOn(dispatcher)
                .onStart {
                    callback?.onStart()
                }
                .onEach {
                    when (it) {
                        is Result.Success -> callback?.onResponse(it.data)
                        is Result.Error -> callback?.onError(it.error)
                    }
                }
                .onCompletion {
                    callback?.onComplete()
                }.collect()
        }
    }

    fun cancel() {
        scope.cancel()
    }

    abstract suspend fun buildUseCase(params: Params?): T
}

private sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val error: Throwable) : Result<Nothing>()
}

class UseCaseBuilder<Response, Params> {
    var showLoading: Boolean = true
    var params: Params? = null
    var onStart: (() -> Unit)? = null
    lateinit var onResponse: (Response) -> Unit
    var onError: ((Throwable) -> Unit)? = null
    var onComplete: (() -> Unit)? = null
}
